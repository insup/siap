-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2016 at 08:03 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pps`
--

-- --------------------------------------------------------

--
-- Table structure for table `dsn`
--

CREATE TABLE IF NOT EXISTS `dsn` (
  `kode_dsn` int(10) NOT NULL,
  `nama_dsn` varchar(75) NOT NULL,
  `pend_akhir` varchar(20) NOT NULL,
  `jab_akademik` varchar(30) NOT NULL,
  `NIP` varchar(30) NOT NULL,
  PRIMARY KEY (`kode_dsn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dsn`
--

INSERT INTO `dsn` (`kode_dsn`, `nama_dsn`, `pend_akhir`, `jab_akademik`, `NIP`) VALUES
(40127, 'Prof. Dr. Rasdi Ekosiswoyo M.Sc.', 'Dr', 'Guru Besar', '194606211973081001'),
(40153, 'Prof. Dr. Dwi Rukmini, M.Pd', 'Dr', 'Guru Besar', '195104151976032001'),
(40155, 'Prof. Dr. Maman Rachman M.Sc.', 'Dr', 'Guru Besar', '194806091976031001'),
(40169, 'Dr. Antonius Tri Widodo, M.Pd', 'Dr', 'Lektor Kepala', '195205201976031004'),
(40177, 'Prof. Nathan Hindratno, Ph.D', 'Dr', 'Guru Besar', '195206131976121002'),
(40182, 'Prof. Dr. Mungin Eddy Wibowo M.Pd., Kons.', 'Dr', 'Guru Besar', '195211201977031002'),
(40185, 'Prof. Dr. Sugiyo, M.Si', 'Dr', 'Guru Besar', '195204111978021001'),
(40195, 'Prof. Dr. Tjetjep Rohendi Rohidi, MA', 'Dr', 'Guru Besar', '194809151979031001'),
(40199, 'Prof. Dr. Hardi Suyitno M.Pd.', 'Dr', 'Guru Besar', '195004251979031001'),
(40206, 'Prof. Dr. Kasmadi Imam Supardi M.S.', 'Dr', 'Guru Besar', '195111151979031001'),
(40231, 'Dr. Khomsun Nurhalim, M.Pd ', 'Dr', 'Lektor Kepala', '195305281980031002'),
(40236, 'Prof. Dr. Kartono, M.Si', 'Dr', 'Guru Besar', '195602221980031002'),
(40237, 'Prof. Dr. Soesanto, M.Pd', 'Dr', 'Guru Besar', '195609011980031004'),
(40250, 'Prof. Dr. Soegiyanto, KS.MS', 'Dr', 'Guru Besar', '195401111981031002'),
(40258, 'Dr. Sri Iswidayati,M.Hum', 'Dr', 'Lektor Kepala', '195207011981112001'),
(40260, 'Prof.Dr. Sudijono Sastroatmodjo M.Si.', 'Dr', 'Guru Besar', '195208151982031007'),
(40262, 'Dr. Partono Thomas, M.S', 'Dr', 'Lektor Kepala', '195212191982031002'),
(40280, 'Dr. Ali Sunarso, M.Pd', 'Dr', 'Lektor Kepala', '196004191983021001'),
(40284, 'Dr. Januarius Mujiyanto, M. Hum', 'Dr', 'Lektor Kepala', '195312131983031002'),
(40286, 'Prof. Dr. Supartono, M.S.', 'Dr', 'Guru Besar', '195412281983031003'),
(40289, 'Prof. Dr. Suyahmo,M.Si', 'Dr', 'Guru Besar', '195503281983031003'),
(40292, 'Prof. Dr. Joko Sutarto, M.Pd', 'Dr', 'Guru Besar', '195609081983031003'),
(40294, 'Dr. Asih Kuswardinah, M.Pd', 'Dr', 'Lektor Kepala', '195707191983032001'),
(40299, 'Prof. Dr. Rustono M.Hum.', 'Dr', 'Guru Besar', '195801271983031003'),
(40300, 'Dr. Sri Sulistyorini, M.Pd', 'Dr', 'Lektor Kepala', '195805171983032002'),
(40302, 'Dr. Sri Haryani, M.Si', 'Dr', 'Lektor Kepala', '195808081983032002'),
(40304, 'Dr. Wahyono, MM', 'Dr', 'Lektor Kepala', '195601031983121001'),
(40318, 'Dr. Dwijanto, M.S.', 'Dr', 'Lektor Kepala', '195804301984031006'),
(40321, 'Prof. YL Sukestiyarno, M.S., Ph.D.', 'Dr', 'Guru Besar', '195904201984031002'),
(40325, 'Dr. Achmad Rifai RC, M.Pd', 'Dr', 'Lektor Kepala', '195908211984031001'),
(40331, 'Prof. Dr.rer.nat. Wahyu Hardyanto M.Si.', 'Dr', 'Guru Besar', '196011241984031002'),
(40332, 'Prof. Dr. Tandiyo Rahayu, M.Pd.', 'Dr', 'Guru Besar', '196103201984032001'),
(40336, 'Prof. Dr. Tri Joko Raharjo, M.Pd', 'Dr', 'Guru Besar', '195903011985111001'),
(40349, 'Prof. Dr. Sugiharto, M.S. ', 'Dr', 'Guru Besar', '195711231985031001'),
(40353, 'Dr. Suwito Eko Pramono, M.Pd', 'Dr', 'Lektor Kepala', '195809201985031003'),
(40361, 'Dr. Achmad Sopyan, M.Pd', 'Dr', 'Lektor', '196006111984031001'),
(40364, 'Prof. Dr. Ani Rusilowati, M.Pd', 'Dr', 'Guru Besar', '196012191985032002'),
(40384, 'Dr. Anwar Sutoyo, M.Pd', 'Dr', 'Lektor', '195811031986011001'),
(40395, 'Dr. Wahyu Lestari, M.Pd', 'Dr', 'Lektor Kepala', '196008171986012001'),
(40396, 'Dr. Enni Suwarsi Rahayu, M.Si', 'Dr', 'Lektor Kepala', '196009161986012001'),
(40405, 'Prof. Dr. Hartono, M.Pd.', 'Dr', 'Guru Besar', '196108101986011001'),
(40407, 'Dr. Setya Rahayu, MS ', 'Dr', 'Lektor Kepala', '196111101986012001'),
(40408, 'Dr. Cahyo Budi Utomo, M.Pd', 'Dr', 'Lektor Kepala', '196111211986011001'),
(40409, 'Prof. Dr Dwi Yuwono Puji Sugiharto M.Pd., Kons', 'Dr', 'Guru Besar', '196112011986011001'),
(40410, 'Dr. Wardono, M.Si', 'Dr', 'Lektor Kepala', '196202071986011001'),
(40411, 'Prof. Dr. Haryono M.Psi.', 'Dr', 'Guru Besar', '196202221986011001'),
(40412, 'Dr. Trisnani Widowati, M.Si', 'Dr', 'Lektor Kepala', '196202271986012001'),
(40416, 'Dr. Kardoyo, M.Pd', 'Dr', 'Lektor Kepala', '196205291986011001'),
(40420, 'Prof. Dr. Fakhruddin, M.Pd', 'Dr', 'Guru Besar', '195604271986031001'),
(40422, 'Dr. Lisdiana, M.Si. ', 'Dr', 'Lektor Kepala', '195911191986032001'),
(40426, 'Dr. Djoko Sutopo, M.Si', 'Dr', 'Lektor Kepala', '195403261986011001'),
(40431, 'Prof. Dr. Ir. Priyantini Widiyaningrum M.S.', 'Dr', 'Guru Besar', '196004191986102001'),
(40432, 'Dr. Rochmad, M.Si', 'Dr', 'Lektor Kepala', '195711161987011001'),
(40440, 'Dr. Agus Wahyudin, M.Si', 'Dr', 'Lektor Kepala', '196208121987021001'),
(40451, 'Dr. Andreas Priyono Budi Prasetyo, M.Ed', 'Dr', 'Lektor', '195811041987031004'),
(40459, 'Prof. Dr. Samsudi M.Pd', 'Dr', 'Guru Besar', '196008081987021001'),
(40463, 'Prof. Dr. Sarwi, M.Si', 'Dr', 'Guru Besar', '196208091987031001'),
(40466, 'Dr. Edy Purwanto, M.Si', 'Dr', 'Lektor Kepala', '196301211987031001'),
(40472, 'Dr. Siti Harnina Bintari, M.Si', 'Dr', 'Lektor Kepala', '196008141987102001'),
(40494, 'Prof. Dr. Muhammad Jazuli M.Hum', 'Dr', 'Guru Besar', '196107041988031003'),
(40499, 'Prof. Dr. Dewi Liesnoor Setyowati M.Si.', 'Dr', 'Guru Besar', '196208111988032001'),
(40508, 'Dr. Putut Marwoto, M.S. ', 'Dr', 'Lektor Kepala', '196308211988031004'),
(40509, 'Prof. Dr. Wiyanto, M.Si.', 'Dr', 'Guru Besar', '196310121988031001'),
(40514, 'Prof. Dr. ZAENURI S.E, M.Si,Akt', 'Dr', 'Guru Besar', ' 196412231988031001 '),
(40520, 'Prof. Dr. Masrukhi M.Pd', 'Dr', 'Guru Besar', '196205081988031002'),
(40525, 'Dr. Dwi Anggani Linggar Bharati, M.Pd', 'Dr', 'Lektor Kepala', '195901141989012001'),
(40527, 'Prof. Dr. Agus Nuryatin M.Hum.', 'Dr', 'Guru Besar', '196008031989011001'),
(40528, 'Dr. Eva Banowati, M.Si', 'Dr', 'Lektor Kepala', '196109291989012003'),
(40532, 'Drs. Ahmad Sofwan Ph.D.', 'Dr', 'Lektor Kepala', '196204271989011001'),
(40533, 'Dr. Sulaiman, M.Pd', 'Dr', 'Lektor Kepala', '196206121989011001'),
(40536, 'Prof. Dr. Etty Soesilowati, M.Si', 'Dr', 'Guru Besar', '196304181989012001'),
(40539, 'Dr. Khumaedi, M.Si', 'Dr', 'Lektor Kepala', '196306101989011002'),
(40544, 'Prof. Dr. Wasino, M.Hum', 'Dr', 'Guru Besar', '196408051989011001'),
(40546, 'Dr. Sunyoto Eko Nugroho, M.Si', 'Dr', 'Lektor Kepala', '196501071989011001'),
(40547, 'Dr. Isti Hidayah, M.Pd. ', 'Dr', 'Lektor Kepala', '196503151989012002'),
(40548, 'Prof. Dr. Tri Marhaeni Pudji Astuti, M.Hum.', 'Dr', 'Guru Besar', '196506091989012001'),
(40553, 'Dr. Mimi Mulyani M.Hum.', 'Dr', 'Lektor Kepala', '196203181989032003'),
(40561, 'Dr. Masrukan, M.si', 'Dr', 'Lektor Kepala', '196604191991021001'),
(40562, 'Dr. Agus Yulianto, M.Si', 'Dr', 'Lektor Kepala', '196607051990031002'),
(40563, 'Dr. Abdurrahman Faridi, M.Pd', 'Dr', 'Lektor Kepala', '195301121990021001'),
(40565, 'Prof. Dr. Teguh Supriyanto, M.Hum. ', 'Dr', 'Lektor Kepala', '196101071990021001'),
(40569, 'Dr. Djuniadi M.T.', 'Dr', 'Lektor Kepala', '196306281990021000'),
(40574, 'Drs. Hartoyo, M.A., Ph.D.', 'Dr', 'Lektor Kepala', '196502231990021001'),
(40580, 'Dr. Ir. Dyah Rini Indriyanti, MP', 'Dr', 'Lektor Kepala', '196304071990032001'),
(40590, 'Prof. Dr. Ir. Amin Retnoningsih, M.Si', 'Dr', 'Guru Besar', '196007121990032001'),
(40592, 'Prof. Dr. Edy Cahyono, M.Si.', 'Dr', 'Guru Besar', '196412051990021001'),
(40593, 'Dr. Ir. Rodia Syamwil, M.Pd', 'Dr', 'Lektor Kepala', '195303211990112001'),
(40595, 'Dr. Scolastika Mariani, M.Si', 'Dr', 'Lektor', '196502101991022001'),
(40596, 'Prof. Dr. Fathur Rokhman, M.Hum', 'Dr', 'Guru Besar', '196612101991031003'),
(40598, 'Dr. Muhammad Khumaedi, M.Pd', 'Dr', 'Lektor Kepala', '196209131991021001'),
(40603, 'Prof. Dr. Totok Sumaryanto, M.Pd', 'Dr', 'Guru Besar', '196410271991021001'),
(40607, 'Prof. Dr. Supriyadi, M.Si', 'Dr', 'Guru Besar', '196505181991021001'),
(40611, 'Dr. Saiful Ridlo, M.Si', 'Dr', 'Lektor Kepala', '196604191991021002'),
(40613, 'Dr. Bernadus Wahyudi Joko Santoso M.Hum', 'Dr', 'Lektor Kepala', '196110261991031001'),
(40614, 'Dr. Hartono, M.Pd', 'Dr', 'Lektor Kepala', '196303041991031000'),
(40620, 'Prof. Dr. Joko Widodo M.Pd.', 'Dr', 'Guru Besar', '196701061991031003'),
(40637, 'Dr. Ngurah Made Darma Putra, M.Si', 'Dr', 'Lektor', '196702171992031002'),
(40645, 'Prof. Dr. Subyantoro, M.Hum. ', 'Dr', 'Guru Besar', '196802131992031002'),
(40647, 'Prof. Dr. Sudarmin, M.S', 'Dr', 'Guru Besar', '196601231992031003'),
(40649, 'Dr. Sugianto, M.Si', 'Dr', 'Lektor Kepala', '196102191993031001'),
(40659, 'Dr. Hari Bakti Mardikantoro, M.Hum', 'Dr', 'Lektor Kepala', '196707261993031004'),
(40670, 'Prof. Dr. St. Budi Waluya, M.Si.', 'Dr', 'Guru Besar', '199505192015071002'),
(40688, 'Dr. Ida Zulaeha, M.Hum', 'Dr', 'Lektor Kepala', '197001091994032001'),
(40696, 'Dr. Suharto Linuwih, M.Si', 'Dr', 'Lektor', '196807141996031005'),
(40700, 'Prof. Dr. Sucihatiningsih Dian Wisika Prajanti M.Si', 'Dr', 'Guru Besar', '196812091997022001'),
(40702, 'Dr. Mulyono, M.Si', 'Dr', 'Lektor', '197009021997021001'),
(40705, 'Dr. drh R. Susanti M.', 'Dr', 'Lektor Kepala', '196903231997032001'),
(40707, 'Dr. MARGARETA RAHAYUNINGSIH , M.Si', 'Dr', 'Lektor Kepala', '197001221997032003'),
(40713, 'Dr. Sulhadi, M.Si', 'Dr', 'Lektor', '197108161998021001'),
(40721, 'Dr. Widiyanto, MBA, MM', 'Dr', 'Lektor Kepala', '196302081998031001'),
(40733, 'Dr. Titi Prihatin, M.Pd', 'Dr', 'Lektor Kepala', '196302121999032001'),
(40743, 'Dr. Sunarto, M.Hum', 'Dr', 'Lektor Kepala', '196912151999031001'),
(40750, 'Dr. Iwan Junaedi, M.Pd', 'Dr', 'Lektor', '197103281999031001'),
(40756, 'Dr. Niken Subekti, M.Si', 'Dr', 'Lektor Kepala', '197302141999032001'),
(40761, 'Prof. Dr. Sutikno, M.T', 'Dr', 'Guru Besar', '197411201999031003'),
(40798, 'Dr. P. Eko Prasetyo, M.Pd', 'Dr', 'Lektor Kepala', '196801022002121003'),
(40799, 'Dr. Rudi Hartono, M.Si', 'Dr', 'Lektor Kepala', '196909072002121001'),
(40804, 'Dr. Indah Sri Utari, SH, M.Hum', 'Dr', 'Lektor', '196401132003122001'),
(40823, 'Prof. Dr. H. Achmad Slamet M.Si.', 'Dr', 'Guru Besar', '196105241986011001'),
(40849, 'Khoirudin Fathoni', 'MT', 'Lektor', '199009292015051901');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_kul`
--

CREATE TABLE IF NOT EXISTS `jadwal_kul` (
  `kode_makul` varchar(10) NOT NULL,
  `kode_dsn` int(10) NOT NULL,
  `kode_jam` varchar(10) NOT NULL,
  `kode_ruang` int(10) NOT NULL,
  `kode_prodi` varchar(10) NOT NULL,
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `jadwal_kul`
--

INSERT INTO `jadwal_kul` (`kode_makul`, `kode_dsn`, `kode_jam`, `kode_ruang`, `kode_prodi`, `id_jadwal`) VALUES
('15P04221', 40127, '101', 1, '100350', 1),
('15P04224', 40849, '201-202', 2, '100350', 3),
('15P04221', 40331, '509', 2, '100350', 4);

-- --------------------------------------------------------

--
-- Table structure for table `jam_kul`
--

CREATE TABLE IF NOT EXISTS `jam_kul` (
  `id_jam` int(10) NOT NULL AUTO_INCREMENT,
  `kode_jam` varchar(10) NOT NULL,
  `hari` varchar(15) NOT NULL,
  `waktu` varchar(20) NOT NULL,
  PRIMARY KEY (`id_jam`),
  UNIQUE KEY `kode_jam` (`kode_jam`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `jam_kul`
--

INSERT INTO `jam_kul` (`id_jam`, `kode_jam`, `hari`, `waktu`) VALUES
(1, '101', 'Senin', '07.00-07.50'),
(2, '101-102', 'Senin', '07.00-08.40'),
(3, '101-103', 'Senin', '07.00-09.30'),
(4, '103', 'Senin', '09.00-09.50'),
(5, '103-104', 'Senin', '09.00-10.40'),
(6, '103-105', 'Senin', '09.00-11.30'),
(7, '105', 'Senin', '11.00-11.50'),
(8, '105-106', 'Senin', '11.00-12.40'),
(9, '105-107', 'Senin', '11.00-13.30'),
(10, '107', 'Senin', '13.00-13.50'),
(11, '107-108', 'Senin', '13.00-14.40'),
(12, '107-109', 'Senin', '13.00-15.30'),
(13, '109', 'Senin', '15.00-15.50'),
(14, '109-110', 'Senin', '15.00-16.40'),
(16, '201', 'Selasa', '07.00-07.50'),
(17, '201-202', 'Selasa', '07.00-08.40'),
(18, '201-203', 'Selasa', '07.00-09.30'),
(19, '203', 'Selasa', '09.00-09.50'),
(20, '203-204', 'Selasa', '09.00-10.40'),
(21, '203-205', 'Selasa', '09.00-11.30'),
(22, '205', 'Selasa', '11.00-11.50'),
(23, '205-206', 'Selasa', '11.00-12.40'),
(24, '205-207', 'Selasa', '11.00-13.30'),
(25, '207', 'Selasa', '13.00-13.50'),
(26, '207-208', 'Selasa', '13.00-14.40'),
(27, '207-209', 'Selasa', '13.00-15.30'),
(28, '209', 'Selasa', '15.00-15.50'),
(29, '209-210', 'Selasa', '15.00-16.40'),
(30, '301', 'Rabu', '07.00-07.50'),
(31, '301-302', 'Rabu', '07.00-08.40'),
(32, '301-303', 'Rabu', '07.00-09.30'),
(33, '303', 'Rabu', '09.00-09.50'),
(34, '303-304', 'Rabu', '09.00-10.40'),
(35, '303-305', 'Rabu', '09.00-11.30'),
(36, '305', 'Rabu', '11.00-11.50'),
(37, '305-306', 'Rabu', '11.00-12.40'),
(38, '305-307', 'Rabu', '11.00-13.30'),
(39, '307', 'Rabu', '13.00-13.50'),
(40, '307-308', 'Rabu', '13.00-14.40'),
(41, '307-309', 'Rabu', '13.00-15.30'),
(42, '309', 'Rabu', '15.00-15.50'),
(43, '309-310', 'Rabu', '15.00-16.40'),
(44, '401', 'Kamis', '07.00-07.50'),
(45, '401-402', 'Kamis', '07.00-08.40'),
(46, '401-403', 'Kamis', '07.00-09.30'),
(47, '403', 'Kamis', '09.00-09.50'),
(48, '403-404', 'Kamis', '09.00-10.40'),
(49, '403-405', 'Kamis', '09.00-11.30'),
(50, '405', 'Kamis', '11.00-11.50'),
(51, '405-406', 'Kamis', '11.00-12.40'),
(52, '405-407', 'Kamis', '11.00-13.30'),
(53, '407', 'Kamis', '13.00-13.50'),
(54, '407-408', 'Kamis', '13.00-14.40'),
(55, '407-409', 'Kamis', '13.00-15.30'),
(56, '409', 'Kamis', '15.00-15.50'),
(57, '409-410', 'Kamis', '15.00-16.40'),
(58, '501', 'Jum''at', '07.00-07.50'),
(59, '501-502', 'Jum''at', '07.00-08.40'),
(60, '501-503', 'Jum''at', '07.00-09.30'),
(61, '503', 'Jum''at', '09.00-09.50'),
(62, '503-504', 'Jum''at', '09.00-10.40'),
(63, '503-505', 'Jum''at', '09.00-11.30'),
(64, '507', 'Jum''at', '13.00-13.50'),
(65, '507-508', 'Jum''at', '13.00-14.40'),
(66, '507-509', 'Jum''at', '13.00-15.30'),
(67, '509', 'Jum''at', '15.00-15.50'),
(68, '509-510', 'Jum''at', '15.00-16.40');

-- --------------------------------------------------------

--
-- Table structure for table `ma_kul`
--

CREATE TABLE IF NOT EXISTS `ma_kul` (
  `kode_makul` varchar(10) NOT NULL,
  `nama_makul` varchar(60) NOT NULL,
  `SKS` int(3) NOT NULL,
  `semester` varchar(10) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `RPS` varchar(100) NOT NULL,
  `kode_prodi` varchar(10) NOT NULL,
  PRIMARY KEY (`kode_makul`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mhs`
--

CREATE TABLE IF NOT EXISTS `mhs` (
  `NIM` varchar(10) NOT NULL,
  `nama_mhs` varchar(30) NOT NULL,
  `alamat_mhs` varchar(100) NOT NULL,
  `tgl_lahir_mhs` date NOT NULL,
  `kode_prodi` varchar(10) NOT NULL,
  PRIMARY KEY (`NIM`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mhs`
--

INSERT INTO `mhs` (`NIM`, `nama_mhs`, `alamat_mhs`, `tgl_lahir_mhs`, `kode_prodi`) VALUES
('5301413048', 'Fathoni Khoirudin', 'Tanah Mas Semarang', '1990-09-29', '530140'),
('5302413048', 'Iin Supriyati', 'Kalongan, RT 04/VIII Desa Kalongan, Kec. Ungaran Timur, Kab. Semarang', '1995-05-19', '530240');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE IF NOT EXISTS `prodi` (
  `kode_prodi` varchar(10) NOT NULL,
  `nama_prodi` varchar(60) NOT NULL,
  PRIMARY KEY (`kode_prodi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`kode_prodi`, `nama_prodi`) VALUES
('100350', 'Penelitian dan Evaluasi Pendidikan'),
('530140', 'PTE'),
('530240', 'PTIK'),
('610240', 'PJPGSD');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE IF NOT EXISTS `ruang` (
  `kode_ruang` int(10) NOT NULL,
  `nama_ruang` varchar(15) NOT NULL,
  PRIMARY KEY (`kode_ruang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`kode_ruang`, `nama_ruang`) VALUES
(1, 'E11-101'),
(2, 'e11-202');

-- --------------------------------------------------------

--
-- Table structure for table `struktur_kurikulum`
--

CREATE TABLE IF NOT EXISTS `struktur_kurikulum` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `kode_makul` varchar(10) NOT NULL,
  `smt` varchar(5) NOT NULL,
  `mata_kuliah` varchar(100) NOT NULL,
  `bobot_sks` int(11) NOT NULL,
  `inti` int(11) NOT NULL,
  `institusional` int(11) NOT NULL,
  `deskripsi` varchar(1000) NOT NULL,
  `silabus` varchar(100) NOT NULL,
  `sap` varchar(100) NOT NULL,
  `prodi` varchar(10) NOT NULL,
  `tahun` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_makul` (`kode_makul`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `struktur_kurikulum`
--

INSERT INTO `struktur_kurikulum` (`id`, `kode_makul`, `smt`, `mata_kuliah`, `bobot_sks`, `inti`, `institusional`, `deskripsi`, `silabus`, `sap`, `prodi`, `tahun`, `status`) VALUES
(1, '15P04221', 'Gasal', 'Filsafat Pendidikan', 3, 2, 1, 'ini adalah mata kuliah filsafat pendidikan', 'bab 2.pdf', 'bab 2.pdf', '100350', 2014, 0),
(2, '15P04222', 'Gasal', 'wet', 12, 2, 10, '5345245', 'PENELITIAN PPS UNNES 2015.pptx', 'Presentation1.pptx', '100350', 2015, 1),
(3, '15P04223', 'Gasal', 'Landasan Kependidikan', 2, 2, 0, 'ini adalah ........', 'bab 2.pdf', 'BAB-II-Page-Down.pdf', '100350', 2010, 1),
(4, '15P04224', 'Gasal', 'Teknologi Informasi', 3, 2, 1, 'jeng jeng jeng......', 'KARINA_cetak form pendaftaran wisuda.pdf', 'Devi AN_form wisuda.pdf', '100350', 2016, 1),
(5, '53024003', 'Gasal', 'mpti', 5, 2, 3, 'sss', 'BUKU 3A-BORANG AKREDITASI PS S2.pdf', 'BUKU 6-MATRIKS PENILAIAN INSTRUMEN AKREDITASI PS S2.pdf', '530240', 2010, 1),
(6, '5302411', 'Gasal', 'PTIK', 3, 2, 1, 'esvesetwetw4t', 'Devi AN_form wisuda.pdf', 'KARINA_cetak form pendaftaran wisuda.pdf', '530240', 2016, 1),
(7, '15P04226', 'Gasal', 'Pengukuran dan Evaluasi', 2, 2, 0, 'inilah ........', 'Transaksi20160103001.pdf', 'Transaksi1231003.pdf', '100350', 2016, 1),
(9, '15P04227', 'Gasal', 'Penelitian Kualitatif', 3, 2, 1, 'bfvh rjhyjttujtuj', 'Transaksi1231011.pdf', 'Transaksi20160103002.pdf', '100350', 2016, 1),
(10, '15P04229', 'Gasal', 'Penelitian Kuantitatif', 3, 3, 0, 'ini adalah.............', '6.Algoritma Kriptografi Klasik (bag 3).ppt', '5Clustering.pdf', '100350', 2016, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tinjauan_kurikulum`
--

CREATE TABLE IF NOT EXISTS `tinjauan_kurikulum` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `prodi` varchar(10) NOT NULL,
  `kode_makul` varchar(10) NOT NULL,
  `status_mk` varchar(20) NOT NULL,
  `perubahan` varchar(100) NOT NULL,
  `alasan` varchar(200) NOT NULL,
  `pengusul` varchar(200) NOT NULL,
  `mulai_smt` varchar(6) NOT NULL,
  `mulai_tahun` int(10) NOT NULL,
  `status` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tinjauan_kurikulum`
--

INSERT INTO `tinjauan_kurikulum` (`id`, `prodi`, `kode_makul`, `status_mk`, `perubahan`, `alasan`, `pengusul`, `mulai_smt`, `mulai_tahun`, `status`) VALUES
(1, '100350', '15P04221', 'Baru', 'Silabus/SAP, Buku Ajar', 'asfasag', 'Wakil Dekan', 'Gasal', 2017, 1),
(2, '100350', '15P04226', 'Baru', 'Silabus/SAP, Buku Ajar', 'sfavbrbrwb', 'Wakil Dekan', 'Gasal', 2016, 1),
(3, '100350', '15P04221', 'Baru', 'Silabus/SAP, Buku Ajar', 'cdvsbsbsfv', 'Wakil Rektor', 'Gasal', 2016, 1),
(4, '100350', '15P04227', 'Baru', 'Buku Ajar, Silabus/SAP', 'avedfqcxwdcqdxd', 'Kajur', 'Gasal', 2016, 1),
(5, '100350', '15P04223', 'Baru', 'Buku Ajar, Silabus/SAP', 'wcascsfwcfe', 'Wakil Dekan', 'Genap', 2016, 1),
(6, '100350', '15P04226', 'Baru', 'Silabus/SAP,', 'seperti itulah intinya', 'Wakil Dekan', 'Genap', 2017, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
