<?php

class DatadosenController extends Controller
{	
	public $layout="main";
	public function actionIndex()
	{
		$criteria = new CDbCriteria();
		$datadsn= Dsn::model()->findAll($criteria);
		$this->render("index", array('datadsn'=>$datadsn));
	}


	public function actionDetail($kode_dsn)
	{
		$connection = Yii::app()->db;
		$sql = "SELECT dsn.kode_dsn, dsn.nama_dsn, dsn.NIP, dsn.pend_akhir, dsn.jab_akademik, dsn.agama, dsn.jen_kel, fakultas.nama_fak as fak, prodi.nama_prodi as prodi, dsn.status_kerja from dsn, prodi, fakultas where dsn.unit_kerja = fakultas.kode_fak && dsn.home_prodi = prodi.kode_prodi && dsn.kode_dsn like '$kode_dsn'";
			
		$command = $connection->createCommand($sql);
		
		$hasil=$command->queryAll();
		$this->render("detail",array("kode_dsn"=>$kode_dsn,"hasil"=>$hasil));
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}