<?php

class InputDatadosenController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionInsertdsn(){
		if($_POST){
		
				$Dsn = new Dsn;

				echo $Dsn->kode_dsn = $_POST['kode_dosen'];
				echo $Dsn->nama_dsn = $_POST['nama_dosen'];
				echo $Dsn->pend_akhir = $_POST['pendidikan'];
				echo $Dsn->jab_akademik = $_POST['jabatan'];
				echo $Dsn->NIP = $_POST['nip'];

				if($Dsn->validate()){
					$Dsn->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					$this->redirect('/siap_p/inputDatadosen/');
				} else {
					Yii::app()->user->setFlash('error','Maaf, simpan gagal');
					$this->redirect(array('/errPage/errDB'));
				}
			
		}
		else $this->actionIndex();
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}