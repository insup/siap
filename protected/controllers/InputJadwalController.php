<?php

class InputJadwalController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql = "select * from prodi";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$prodi = $command->queryAll();

		$sql1 = "select * from struktur_kurikulum";
		$command = $connection->createCommand($sql1);
		$makul = $command->queryAll();

		$sql2 = "select * from dsn";
		$command = $connection->createCommand($sql2);
		$dosen = $command->queryAll();

		$sql3 = "select * from jam_kul";
		$command = $connection->createCommand($sql3);
		$JamKul = $command->queryAll();

		$sql4 = "select * from ruang";
		$command = $connection->createCommand($sql4);
		$Ruang = $command->queryAll();

		$this->render('index', array('prodi'=>$prodi, 'makul'=>$makul, 'dosen'=>$dosen, 'JamKul'=>$JamKul, 'Ruang'=>$Ruang));
	}

	public function actionInsertjdw(){
		if($_POST){
		
				$JadwalKul = new JadwalKul;

				echo $JadwalKul->kode_makul = $_POST['makul'];
				echo $JadwalKul->kode_dsn = $_POST['dosen'];
				echo $JadwalKul->kode_jam = $_POST['kode_jdw'];
				echo $JadwalKul->kode_ruang = $_POST['ruang'];
				echo $JadwalKul->kode_prodi = $_POST['prodi'];

				if($JadwalKul->validate()){
					$JadwalKul->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					$this->redirect('/siap_p/inputJadwal');
				} else {
					Yii::app()->user->setFlash('error','Maaf, simpan gagal');
					$this->redirect(array('/errPage/errDB'));
				}
			
		}
		else $this->actionIndex();
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}