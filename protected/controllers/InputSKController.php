<?php

class InputSKController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql= "select * from prodi";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);

		$hasil = $command->queryAll();

		$this->render('index', array('hasil'=>$hasil));
	}
	
	public function actionInsertSk(){
		if($_POST){
		
				$StrukturKurikulum = new StrukturKurikulum;

				$StrukturKurikulum->smt = $_POST['smt'];
				$StrukturKurikulum->kode_makul = $_POST['kode_makul'];
				$StrukturKurikulum->mata_kuliah = $_POST['mata_kuliah'];
				$StrukturKurikulum->bobot_sks = $_POST['bobot_sks'];
				$StrukturKurikulum->inti = $_POST['inti'];
				$StrukturKurikulum->institusional = $_POST['institusional'];
				$StrukturKurikulum->deskripsi = $_POST['deskripsi'];
				$StrukturKurikulum->silabus = $_POST['silabus'];
				$StrukturKurikulum->sap = $_POST['sap'];
				$StrukturKurikulum->prodi = $_POST['kode_prodi'];
				$StrukturKurikulum->tahun = $_POST['tahun'];
				$StrukturKurikulum->status = 1;

				if($StrukturKurikulum->validate()){
					$StrukturKurikulum->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					//$this->redirect('/siap_p/inputSk/');
				} else {
					Yii::app()->user->setFlash('error','Maaf, simpan gagal');
					$this->redirect(array('/errPage/errDB'));
				}
			
		}
		else $this->actionIndex();
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}