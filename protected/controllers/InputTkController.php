<?php

class InputTkController extends Controller
{
	public $layout="main";
	public function actionIndex()
	{
		$sql= "select * from prodi";
		$sql2 = "select * from struktur_kurikulum";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);
		$command2 = $connection->createCommand($sql2);

		$hasil2 = $command2->queryAll();
		$hasil = $command->queryAll();

		$this->render('index', array('hasil'=>$hasil, 'hasil2'=>$hasil2));
	}

	public function actionInsertTk(){
		if($_POST && isset($_POST['perubahan'])){
				$perubahan_array=$_POST['perubahan'];
				$perubahan = "";
				foreach ($perubahan_array as $key) {
					$perubahan = $key.",".$perubahan;
				}
				
					
				$TinjauanKurikulum = new TinjauanKurikulum;

				echo $TinjauanKurikulum->prodi = $_POST['prodi'];
				echo $TinjauanKurikulum->kode_makul = $_POST['kode_makul'];
				echo $TinjauanKurikulum->status_mk = $_POST['status_makul'];
				echo $TinjauanKurikulum->perubahan = $perubahan;
				echo $TinjauanKurikulum->alasan = $_POST['alasan'];
				echo $TinjauanKurikulum->pengusul = $_POST['pengusul'];
				echo $TinjauanKurikulum->mulai_smt = $_POST['smt'];
				echo $TinjauanKurikulum->mulai_tahun = $_POST['tahun'];
				echo $TinjauanKurikulum->status = 1;

				if($TinjauanKurikulum->validate()){
					$TinjauanKurikulum->save();
					Yii::app()->user->setFlash('success','Selamat, Input Berhasil diSimpan');
					$this->redirect('/siap_p/inputTk/');
				} else {
					Yii::app()->user->setFlash('error','Maaf, simpan gagal');
					$this->redirect(array('/errPage/errDB'));
				}
			
		}
		else echo "harusnya dicentang";
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}