<?php

class LihatSKController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql= "select * from prodi";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);

		$hasil = $command->queryAll();

		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionopenData($prodi=null)
	{
		$sql1= "select * from prodi";
		$connection = Yii::app()->db;
		$command1 = $connection->createCommand($sql1);

		$hasil1 = $command1->queryAll();
		$pro = $_POST['prodi'];
		if($prodi==null)
		{
			
			$connection = Yii::app()->db;
			$sql = "SELECT id, mata_kuliah as makul, bobot_sks as sks, smt as smt, prodi.nama_prodi as prodi 
			FROM struktur_kurikulum, prodi WHERE struktur_kurikulum.prodi=prodi.kode_prodi && prodi.kode_prodi like '$pro' && struktur_kurikulum.status=1";
			
			$command = $connection->createCommand($sql);
			//$command->bindParam(':ID',$_GET['prodi'],PDO::PARAM_STR);

			$hasil=$command->queryAll();
			$this->render("openData",array("pro"=>$pro,"hasil"=>$hasil, "hasil1"=>$hasil1));
				
		}
		
		else {
		    $connection = Yii::app()->db;
			$sql = "SELECT id, mata_kuliah as makul, bobot_sks as sks, smt as smt, prodi.nama_prodi as prodi 
			FROM struktur_kurikulum, prodi WHERE struktur_kurikulum.prodi=prodi.kode_prodi && prodi.kode_prodi like '$pro' && struktur_kurikulum.status=1";
				
				
			$command = $connection->createCommand($sql);
			//$command->bindParam(':ID',$_POST['prodi'],PDO::PARAM_STR);
			
			$hasil=$command->queryAll();
			$this->render("openData",array("pro"=>$pro,"hasil"=>$hasil, "hasil1"=>$hasil1));
		}
	}

	public function actionDetail($id)
	{
		$connection = Yii::app()->db;
		$sql = "SELECT kode_makul, mata_kuliah, bobot_sks, smt, inti, institusional, deskripsi, silabus, sap, tahun, prodi.nama_prodi as prodi 
			FROM struktur_kurikulum, prodi WHERE struktur_kurikulum.id = '$id' && struktur_kurikulum.prodi = prodi.kode_prodi";
			
		$command = $connection->createCommand($sql);
		
		$hasil=$command->queryAll();
		$this->render("detail",array("id"=>$id,"hasil"=>$hasil));
	}

	public function actionHapus($id)
	{
		$connection = Yii::app()->db;
		$sql = "Update struktur_kurikulum set status=0 where struktur_kurikulum.id = '$id'";
		$command = $connection->createCommand($sql);
		$hasil = $command->execute();
		$this->redirect('/siap_p/lihatSK');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}