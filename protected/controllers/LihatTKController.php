<?php

class LihatTKController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql = "select* from prodi";
	
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);

		$hasil = $command->queryAll();

		$this->render('index', array('hasil'=>$hasil));
	}

	public function actionopenData($prodi=null)
	{
		$sql1= "select * from prodi";
		$connection = Yii::app()->db;
		$command1 = $connection->createCommand($sql1);

		$hasil1 = $command1->queryAll();
		$pro = $_POST['prodi'];
		if($prodi==null)
		{
			
			$connection = Yii::app()->db;
			$sql= "select prodi.nama_prodi as prodi, struktur_kurikulum.mata_kuliah as makul, status_mk as status, perubahan, tinjauan_kurikulum.id from prodi, struktur_kurikulum, tinjauan_kurikulum where prodi.kode_prodi=tinjauan_kurikulum.prodi && struktur_kurikulum.kode_makul=tinjauan_kurikulum.kode_makul && prodi.kode_prodi like '$pro' && tinjauan_kurikulum.status=1";
			$command = $connection->createCommand($sql);
			//$command->bindParam(':ID',$_GET['prodi'],PDO::PARAM_STR);

			$hasil=$command->queryAll();
			$this->render("openData",array("pro"=>$pro,"hasil"=>$hasil, "hasil1"=>$hasil1));
				
		}
		
		else {
		    $connection = Yii::app()->db;
			$sql= "select prodi.nama_prodi as prodi, struktur_kurikulum.mata_kuliah as makul, status_mk as status, perubahan, tinjauan_kurikulum.id from prodi, struktur_kurikulum, tinjauan_kurikulum where prodi.kode_prodi=tinjauan_kurikulum.prodi && struktur_kurikulum.kode_makul=tinjauan_kurikulum.kode_makul && prodi.kode_prodi like '$pro'";
				
			$command = $connection->createCommand($sql);
			//$command->bindParam(':ID',$_POST['prodi'],PDO::PARAM_STR);
			
			$hasil=$command->queryAll();
			$this->render("openData",array("pro"=>$pro,"hasil"=>$hasil, "hasil1"=>$hasil1));
		}
	}

	public function actionDetail($id)
	{
		$connection = Yii::app()->db;
		$sql = "SELECT struktur_kurikulum.mata_kuliah, pengusul, perubahan, alasan, status_mk as status, mulai_tahun, mulai_smt, prodi.nama_prodi as prodi 
			FROM tinjauan_kurikulum, struktur_kurikulum, prodi WHERE tinjauan_kurikulum.id = '$id' && tinjauan_kurikulum.prodi = prodi.kode_prodi && tinjauan_kurikulum.kode_makul=struktur_kurikulum.kode_makul";
			
		$command = $connection->createCommand($sql);
		
		$hasil=$command->queryAll();
		$this->render("detail",array("id"=>$id,"hasil"=>$hasil));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}