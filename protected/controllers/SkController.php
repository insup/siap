<?php

class SkController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql= "SELECT ma_kul.nama_makul as makul, dsn.nama_dsn as dsn, jam_kul.hari as hari, jam_kul.waktu as waktu, ruang.nama_ruang as ruang, prodi.nama_prodi as prodi FROM ma_kul, dsn, jam_kul, ruang, prodi, jadwal_kul WHERE ma_kul.kode_makul=jadwal_kul.kode_makul && dsn.kode_dsn=jadwal_kul.kode_dsn && jam_kul.id_jam=jadwal_kul.kode_jam && ruang.kode_ruang = jadwal_kul.kode_ruang && prodi.kode_prodi=jadwal_kul.kode_prodi";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);

		$hasil = $command->queryAll();

		$this->render('index', array('hasil'=>$hasil));

	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}