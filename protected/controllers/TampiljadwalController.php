<?php

class TampiljadwalController extends Controller
{
	public $layout = "main";
	public function actionIndex()
	{
		$sql= "SELECT struktur_kurikulum.smt as smt, struktur_kurikulum.mata_kuliah as makul, dsn.nama_dsn as dsn, jam_kul.hari as hari, jam_kul.waktu as waktu, ruang.nama_ruang as ruang, prodi.nama_prodi as prodi FROM struktur_kurikulum, dsn, jam_kul, ruang, prodi, jadwal_kul WHERE struktur_kurikulum.kode_makul=jadwal_kul.kode_makul && jadwal_kul.kode_dsn=dsn.kode_dsn && jadwal_kul.kode_jam=jam_kul.kode_jam && jadwal_kul.kode_ruang=ruang.kode_ruang && jadwal_kul.kode_prodi=prodi.kode_prodi";
		$connection = Yii::app()->db;
		$command = $connection->createCommand($sql);

		$hasil = $command->queryAll();

		$this->render('index', array('hasil'=>$hasil));

	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}