<?php

/**
 * This is the model class for table "dsn".
 *
 * The followings are the available columns in table 'dsn':
 * @property integer $id
 * @property integer $kode_dsn
 * @property string $nama_dsn
 * @property string $pend_akhir
 * @property string $jab_akademik
 * @property string $NIP
 * @property string $agama
 * @property string $jen_kel
 * @property integer $unit_kerja
 * @property string $home_prodi
 * @property string $status_kerja
 */
class Dsn extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dsn';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_dsn, nama_dsn, pend_akhir, jab_akademik, NIP, agama, jen_kel, unit_kerja, home_prodi, status_kerja', 'required'),
			array('kode_dsn, unit_kerja', 'numerical', 'integerOnly'=>true),
			array('nama_dsn', 'length', 'max'=>75),
			array('pend_akhir, jen_kel', 'length', 'max'=>20),
			array('jab_akademik, NIP', 'length', 'max'=>30),
			array('agama', 'length', 'max'=>40),
			array('home_prodi, status_kerja', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kode_dsn, nama_dsn, pend_akhir, jab_akademik, NIP, agama, jen_kel, unit_kerja, home_prodi, status_kerja', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kode_dsn' => 'Kode Dsn',
			'nama_dsn' => 'Nama Dsn',
			'pend_akhir' => 'Pend Akhir',
			'jab_akademik' => 'Jab Akademik',
			'NIP' => 'Nip',
			'agama' => 'Agama',
			'jen_kel' => 'Jen Kel',
			'unit_kerja' => 'Unit Kerja',
			'home_prodi' => 'Home Prodi',
			'status_kerja' => 'Status Kerja',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kode_dsn',$this->kode_dsn);
		$criteria->compare('nama_dsn',$this->nama_dsn,true);
		$criteria->compare('pend_akhir',$this->pend_akhir,true);
		$criteria->compare('jab_akademik',$this->jab_akademik,true);
		$criteria->compare('NIP',$this->NIP,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('jen_kel',$this->jen_kel,true);
		$criteria->compare('unit_kerja',$this->unit_kerja);
		$criteria->compare('home_prodi',$this->home_prodi,true);
		$criteria->compare('status_kerja',$this->status_kerja,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Dsn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
