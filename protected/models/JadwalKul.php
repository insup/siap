<?php

/**
 * This is the model class for table "jadwal_kul".
 *
 * The followings are the available columns in table 'jadwal_kul':
 * @property string $kode_makul
 * @property integer $kode_dsn
 * @property string $kode_jam
 * @property integer $kode_ruang
 * @property string $kode_prodi
 * @property integer $id_jadwal
 */
class JadwalKul extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jadwal_kul';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_makul, kode_dsn, kode_jam, kode_ruang, kode_prodi', 'required'),
			array('kode_dsn, kode_ruang', 'numerical', 'integerOnly'=>true),
			array('kode_makul, kode_jam, kode_prodi', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kode_makul, kode_dsn, kode_jam, kode_ruang, kode_prodi, id_jadwal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kode_makul' => 'Kode Makul',
			'kode_dsn' => 'Kode Dsn',
			'kode_jam' => 'Kode Jam',
			'kode_ruang' => 'Kode Ruang',
			'kode_prodi' => 'Kode Prodi',
			'id_jadwal' => 'Id Jadwal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kode_makul',$this->kode_makul,true);
		$criteria->compare('kode_dsn',$this->kode_dsn);
		$criteria->compare('kode_jam',$this->kode_jam,true);
		$criteria->compare('kode_ruang',$this->kode_ruang);
		$criteria->compare('kode_prodi',$this->kode_prodi,true);
		$criteria->compare('id_jadwal',$this->id_jadwal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JadwalKul the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
