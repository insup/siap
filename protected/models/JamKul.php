<?php

/**
 * This is the model class for table "jam_kul".
 *
 * The followings are the available columns in table 'jam_kul':
 * @property integer $id_jam
 * @property string $kode_jam
 * @property string $hari
 * @property string $jam
 */
class JamKul extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jam_kul';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_jam, hari, jam', 'required'),
			array('kode_jam', 'length', 'max'=>10),
			array('hari', 'length', 'max'=>15),
			array('jam', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_jam, kode_jam, hari, jam', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_jam' => 'Id Jam',
			'kode_jam' => 'Kode Jam',
			'hari' => 'Hari',
			'jam' => 'Jam',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_jam',$this->id_jam);
		$criteria->compare('kode_jam',$this->kode_jam,true);
		$criteria->compare('hari',$this->hari,true);
		$criteria->compare('jam',$this->jam,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JamKul the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
