<?php

/**
 * This is the model class for table "struktur_kurikulum".
 *
 * The followings are the available columns in table 'struktur_kurikulum':
 * @property integer $id
 * @property string $kode_makul
 * @property string $smt
 * @property string $mata_kuliah
 * @property integer $bobot_sks
 * @property integer $inti
 * @property integer $institusional
 * @property string $deskripsi
 * @property string $silabus
 * @property string $sap
 * @property string $prodi
 * @property integer $tahun
 * @property integer $status
 */
class StrukturKurikulum extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'struktur_kurikulum';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kode_makul, smt, mata_kuliah, bobot_sks, inti, institusional, deskripsi, silabus, sap, prodi, tahun, status', 'required'),
			array('bobot_sks, inti, institusional, tahun, status', 'numerical', 'integerOnly'=>true),
			array('kode_makul, prodi', 'length', 'max'=>10),
			array('smt', 'length', 'max'=>5),
			array('mata_kuliah, silabus, sap', 'length', 'max'=>100),
			array('deskripsi', 'length', 'max'=>1000),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, kode_makul, smt, mata_kuliah, bobot_sks, inti, institusional, deskripsi, silabus, sap, prodi, tahun, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'kode_makul' => 'Kode Makul',
			'smt' => 'Smt',
			'mata_kuliah' => 'Mata Kuliah',
			'bobot_sks' => 'Bobot Sks',
			'inti' => 'Inti',
			'institusional' => 'Institusional',
			'deskripsi' => 'Deskripsi',
			'silabus' => 'Silabus',
			'sap' => 'Sap',
			'prodi' => 'Prodi',
			'tahun' => 'Tahun',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('kode_makul',$this->kode_makul,true);
		$criteria->compare('smt',$this->smt,true);
		$criteria->compare('mata_kuliah',$this->mata_kuliah,true);
		$criteria->compare('bobot_sks',$this->bobot_sks);
		$criteria->compare('inti',$this->inti);
		$criteria->compare('institusional',$this->institusional);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('silabus',$this->silabus,true);
		$criteria->compare('sap',$this->sap,true);
		$criteria->compare('prodi',$this->prodi,true);
		$criteria->compare('tahun',$this->tahun);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StrukturKurikulum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
