<?php

/**
 * This is the model class for table "tinjauan_kurikulum".
 *
 * The followings are the available columns in table 'tinjauan_kurikulum':
 * @property integer $id
 * @property string $prodi
 * @property string $kode_makul
 * @property string $status_mk
 * @property string $perubahan
 * @property string $alasan
 * @property string $pengusul
 * @property string $mulai_smt
 * @property integer $mulai_tahun
 */
class TinjauanKurikulum extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tinjauan_kurikulum';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prodi, kode_makul, status_mk, perubahan, alasan, pengusul, mulai_smt, mulai_tahun', 'required'),
			array('mulai_tahun', 'numerical', 'integerOnly'=>true),
			array('prodi, kode_makul', 'length', 'max'=>10),
			array('status_mk', 'length', 'max'=>20),
			array('perubahan', 'length', 'max'=>100),
			array('alasan, pengusul', 'length', 'max'=>200),
			array('mulai_smt', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, prodi, kode_makul, status_mk, perubahan, alasan, pengusul, mulai_smt, mulai_tahun', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'prodi' => 'Prodi',
			'kode_makul' => 'Kode Makul',
			'status_mk' => 'Status Mk',
			'perubahan' => 'Perubahan',
			'alasan' => 'Alasan',
			'pengusul' => 'Pengusul',
			'mulai_smt' => 'Mulai Smt',
			'mulai_tahun' => 'Mulai Tahun',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('prodi',$this->prodi,true);
		$criteria->compare('kode_makul',$this->kode_makul,true);
		$criteria->compare('status_mk',$this->status_mk,true);
		$criteria->compare('perubahan',$this->perubahan,true);
		$criteria->compare('alasan',$this->alasan,true);
		$criteria->compare('pengusul',$this->pengusul,true);
		$criteria->compare('mulai_smt',$this->mulai_smt,true);
		$criteria->compare('mulai_tahun',$this->mulai_tahun);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TinjauanKurikulum the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
