
<div class="panel panel-info">
    <div class="panel-heading">
        Detail Data Dosen
	</div>

    <div class="panel-body">
		<table class="table table-bordered">
			<?php foreach ($hasil as $key): ?>
			<tr>
				<td>Kode Dosen Unnes</td>
				<td><?php echo $key['kode_dsn'] ?></td>
			</tr>
			<tr>
				<td>Nama Dosen</td>
				<td><?php echo $key['nama_dsn'] ?></td>
			</tr>
			<tr>
				<td>Pendidikan Terakhir</td>
				<td><?php echo $key['pend_akhir'] ?></td>
			</tr>
			<tr>
				<td>Jabatan Akademik</td>
				<td><?php echo $key['jab_akademik'] ?></td>
			</tr>
			<tr>
				<td>NIP</td>
				<td><?php echo $key['NIP'] ?></td>
			</tr>
			<tr>
				<td>Agama</td>
				<td><?php echo $key['agama'] ?></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td><?php echo $key['jen_kel'] ?></td>
			</tr>
			<tr>
				<td>Unit Kerja</td>
				<td><?php echo $key['fak'] ?></td>
			</tr>
			<tr>
				<td>Homebase Prodi</td>
				<td><?php echo $key['prodi'] ?></td>
			</tr>
			<tr>
				<td>Status Kerja</td>
				<td><?php echo $key['status'] ?></td>
			</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>