<?php
/* @var $this DatadosenController */

$this->breadcrumbs=array(
	'Datadosen',
);
?>
<h1><center>Data Dosen Pascasarjana Unnes</center></h1>

<table class="table table-striped">
	<thead>
		<tr>
			<th>No.</th>
			<th>Kode Dosen</th>
			<th>Nama Dosen</th>
			<th>Pendidikan Tertinggi</th>
			<th>Jabatan Akademik</th>
			<th>NIP</th>
		</tr>
	</thead>
	<tbody>
		<?php $i=0; ?>
		<?php foreach ($datadsn as $key): ?>
		<?php $i++; ?>
	<tr>
		<td><?php echo $i;?></td>
		<td><a href="<?php echo Yii::app()->request->baseUrl; ?>/datadosen/detail/<?php echo $key['kode_dsn'] ?>" class="btn"><?php echo $key->kode_dsn ?></td>
		<td ><?php echo $key->nama_dsn ?></td>
		<td><?php echo $key->pend_akhir ?></td>
		<td><?php echo $key->jab_akademik ?></td>
		<td><?php echo $key->NIP ?></td>
	</tr>
	</tbody>
	<?php endforeach ?>
</table>
