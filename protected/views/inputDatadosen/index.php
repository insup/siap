<?php
/* @var $this InputSKController */

$this->breadcrumbs=array(
	'Input Data Dosen',
);
?>
<div class="panel panel-info">
    <div class="panel-heading">
        Input Data Dosen
    </div>
<div class="panel-body">
<table>
	<form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/inputDatadosen/Insertdsn">
		<tr><div class="form-group">
            <td><label>Kode Dosen</label></td>
			<td>:</td>											
            <td><input class="form-control" type="text" name="kode_dosen"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Nama Dosen</label></td>
			<td>:</td>											
            <td><input class="form-control" type="text" name="nama_dosen"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Pendidikan Tertinggi</label></td>
			<td>:</td>											
            <td><input class="form-control" type="text" name="pendidikan"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Jabatan Akademik</label></td>
			<td>:</td>											
            <td><input class="form-control" type="text" name="jabatan"></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>NIP</label></td>
			<td>:</td>											
            <td><input class="form-control" type="text" name="nip"></td>
            </div>
        </tr>
</table>	
<center><button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>SAVE</button></center>					