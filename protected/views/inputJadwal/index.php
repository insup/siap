<?php
/* @var $this InputSKController */

$this->breadcrumbs=array(
	'Input Jadwal Kuliah',
);
?>
<div class="panel panel-info">
    <div class="panel-heading">
        Input Jadwal Kuliah
    </div>
<div class="panel-body">
<table>
	<form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/inputJadwal/Insertjdw">
		<tr><div class="form-group">
            <td><label>Prodi</label></td>
			<td>:</td>											
            <td><select id="prodi" name="prodi">
				<?php foreach ($prodi as $key): ?>
					<option value="<?php echo $key['kode_prodi']; ?>"><?php echo $key['nama_prodi'] ?></option>
				<?php endforeach ?>
			</select></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Nama Mata Kuliah</label></td>
			<td>:</td>											
            <td><select id="makul" name="makul">
				<?php foreach ($makul as $key): ?>
					<option value="<?php echo $key['kode_makul']; ?>"><?php echo $key['mata_kuliah'] ?></option>
				<?php endforeach ?>
			</select></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Nama Dosen</label></td>
			<td>:</td>											
            <td><select id="dosen" name="dosen">
				<?php foreach ($dosen as $key): ?>
					<option value="<?php echo $key['kode_dsn']; ?>"><?php echo $key['nama_dsn'] ?></option>
				<?php endforeach ?>
			</select></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Kode Jadwal</label></td>
			<td>:</td>											
            <td><select id="kode_jdw" name="kode_jdw">
				<?php foreach ($JamKul as $key): ?>
					<option value="<?php echo $key['kode_jam']; ?>"><?php echo $key['kode_jam']; ?></option>
				<?php endforeach ?>
			</select></td>
            </div>
        </tr>
        <tr><div class="form-group">
            <td><label>Ruang</label></td>
			<td>:</td>											
            <td><select id="ruang" name="ruang">
				<?php foreach ($Ruang as $key): ?>
					<option value="<?php echo $key['kode_ruang']; ?>"><?php echo $key['nama_ruang']; ?></option>
				<?php endforeach ?>
			</select></td>
            </div>
        </tr>
</table>	
<center><button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>SAVE</button></center>					