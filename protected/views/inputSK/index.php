<?php
/* @var $this InputSKController */

$this->breadcrumbs=array(
	'Input Sk',
);
?>
<div class="panel panel-info">
                        <div class="panel-heading">
                           Input Struktur Kurikulum
                        </div>

                        <div class="panel-body">
						
						<table>
                            <form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/InputSk/Insertsk">
                                       <tr> <div class="form-group">
										<td width=130px><label>Semester</label></td>
										<td width=20px>:</td>
                                            <td><select class="form-control" name="smt">
                                                <option>Gasal</option>
                                                <option>Genap</option>
                                            </select></td>
                                        </div></tr>										
										<tr><td> </td></tr>
										<tr><div class="form-group">
                                            <td><label>Kode MK</label></td>
											<td>:</td>											
                                            <td><input class="form-control" type="text" name="kode_makul"></td>
                                        </div></tr>
										
										<tr><div class="form-group">
                                            <td><label>Mata Kuliah</label></td>
											<td>:</td>											
                                            <td><input class="form-control" type="text" name="mata_kuliah"></td>                                     
                                        </div></tr>
										
										<tr><div class="form-group">
                                            <td><label>Bobot SKS</label></td>
											<td>:</td>											
                                            <td><input class="form-control" type="text" name="bobot_sks"></td>										
                                        </div></tr>
                                 
									</table>
									<hr/>
									
									
									<div class="form-group"><label>SKS MK dalam Kurikulum</label></div>									
									<table>
									
										<tr><div class="form-group">
                                            <td width=130px><label>Inti</label></td>
											<td width=20px>:</td>											
                                            <td><input class="form-control" type="text" name="inti"></td>                                     
                                        </div></tr>
										
										<tr><div class="form-group">
                                            <td><label>Institusional</label></td>
											<td>:</td>											
                                            <td><input class="form-control" type="text" name="institusional"></td>                                     
                                        </div></tr>							                             
									</table>
									<hr/>
									
									<div class="form-group"><label>Kelengkapan</label></div>									
									<table>
										<tr><div class="form-group">
                                            <td width=130px><label>Deskripsi</label></td>
											<td width=20px>:</td>
                                            <td><textarea class="form-control" rows="3" name="deskripsi"></textarea></td>
                                        </div></tr>
									<tr><div class="form-group">
									<td><label class="control-label col-lg-4">Silabus</label></td>
									<td>:</td>
									<td>
									<div class="">
										<div class="fileupload fileupload-new" data-provides="fileupload">
											<span class="btn btn-file btn-default">
												<span class="fileupload-new">Select file</span>
												<span class="fileupload-exists">Change</span>
												<input type="file" name="silabus">
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
										</div>
									</div></td>
									</div></tr>
									<tr><div class="form-group">
									<td><label class="control-label col-lg-4">SAP</label></td>
									<td>:</td>
									<td>
									<div class="">
										<div class="fileupload fileupload-new" data-provides="fileupload">
											<span class="btn btn-file btn-default">
												<span class="fileupload-new">Select file</span>
												<span class="fileupload-exists">Change</span>
												<input type="file" name="sap">
											</span>
											<span class="fileupload-preview"></span>
											<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
										</div>
									</div></td>
									</div></tr>	
									
									</table>
									<hr/>
									
									<table>
									
										<tr><div class="form-group">
                                            <td><label>Unit penyelenggara</label></td>
											<td width=20px>:</td>
											<td><select id="kode_prodi" name="kode_prodi">
												<?php foreach ($hasil as $key): ?>
													<option value="<?php echo $key['kode_prodi']; ?>"><?php echo $key['nama_prodi'] ?></option>
												<?php endforeach ?>
											</select></td>									                                   
                                        </div></tr>
										<tr><div class="form-group">
										<td><label>Tahun</label></td>
											<td width=20px>:</td>
										<td><select class="form-control" placeholder="Tahun" name="tahun">
                                                <?php for ($i=date('Y'); $i > date('Y')-10; $i--) { 
													echo "<option value=$i>$i</option>";
												} ?>
                                            </select></td>
										</div></tr>
									</table>
									<br/>
									<center><button type="submit" class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>SAVE</button></center>
									
                            </div>
							
                        </div>