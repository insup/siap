<?php
/* @var $this InputTkController */

$this->breadcrumbs=array(
	'Input Tk',
);
?>

<div class="panel panel-info">
                        <div class="panel-heading">
                           Input Tinjauan Kurikulum
                        </div>
                        <div class="panel-body">
						<table>
                            <form method="post" role="form" action="<?php echo Yii::app()->request->baseUrl; ?>/inputTK/InsertTK">
                                       <tr><div class="form-group">
                                            <td><label>Prodi</label></td>
											<td width=20px>:</td>
											<td><select class="form-control" id="prodi" name="prodi" placeholder="Prodi" >
												<?php foreach ($hasil as $key): ?>
													<option value="<?php echo $key['kode_prodi']; ?>"><?php echo $key['nama_prodi'] ?></option>
												<?php endforeach ?>
											</select></td>									                                   
                                        </div></tr>										
										<tr><div class="form-group">
                                            <td><label>Nama Mata Kuliah</label></td>
											<td>:</td>											
                                            <td><select class="form-control" id="kode_makul" name="kode_makul">
												<?php foreach ($hasil2 as $key2): ?>
													<option value="<?php echo $key2['kode_makul']; ?>"><?php echo $key2['mata_kuliah'] ?></option>
												<?php endforeach ?>
											</select></td>	                                     
                                        </div></tr>
										
										<tr><div class="form-group">
                                            <td><label>MK Baru/ Lama/ Hapus</label></td>
											<td>:</td>											
                                            <td><select class="form-control" id="status_makul" name="status_makul">
                                                <option>Baru</option>
                                                <option>Lama</option>
                                                <option>Hapus</option>
											</select></td>                                     
                                        </div></tr>
										
										<tr><div class="form-group">
                                            <td><label>Perubahan pada</label></td>
											<td>:</td>											
                                            <td><textarea class="form-control" rows="3" id="perubahan" name="perubahan" placeholder="isikan perubahan pada SILABUS/SAP atau Materi Ajar yang diharapkan"></textarea></td>
                                        </div></tr>
										
										<tr><div class="form-group">
                                            <td width=130px><label>Alasan Peninjauan</label></td>
											<td width=20px>:</td>
                                            <td><textarea class="form-control" rows="3" id="alasan" name="alasan"></textarea></td>
                                        </div></tr>
										<tr> <div class="form-group">
										<td width=130px><label>Atas Usulan / Masukan dari</label></td>
										<td width=20px>:</td>
                                            <td><select class="form-control" id="pengusul" name="pengusul">
                                                <option>Mahasiswa</option>
                                                <option>Alumni</option>
                                                <option>Masyarakat</option>
                                                <option>Kaprodi</option>
                                                <option>Kajur</option>
                                                <option>Wakil Dekan</option>
                                                <option>Wakil Rektor</option>
                                            </select></td>
                                        </div></tr>
										<tr> <div class="form-group">
										<td width=130px><label>Berlaku Mulai</label></td>
										<td width=20px>:</td>
                                            <td><select class="form-control" id="smt" name="smt">
                                                <option>--semester--</option>
												<option>Gasal</option>
                                                <option>Genap</option>
                                            </select></td>
											<td><select class="form-control" placeholder="Tahun" id="tahun" name="tahun">
                                                <?php for ($i=date('Y'); $i < date('Y')+3; $i++) { 
													echo "<option value=$i>$i</option>";
												} ?>
                                            </select></td>
                                        </div></tr>
										
									</table>
									<br/>

									<center><button class="btn btn-inverse"><i class="glyphicon glyphicon-upload"></i>SAVE</button></center>
									
                            </div>
							
                        </div>