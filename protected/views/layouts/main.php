<?php /* @var $this Controller */ ?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SIAP UNNES</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/font-awesome.css" rel="stylesheet" />
       <!--CUSTOM BASIC STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/basic.css" rel="stylesheet" />
    <!--CUSTOM MAIN STYLES-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="nav-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header tengahin">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand " href="<?php echo Yii::app()->request->baseUrl; ?>/home"><font color="white" size="6px">SIAP UNNES</font></a>
            </div>

            <div class="header-right">
                <font size="4.5px"><marquee>Sistem Informasi Akademik Pascasarjana UNNES</marquee></font>
            </div>
            <!-- <div class="header-right">

                <a href="message-task.html" class="btn btn-info" title="New Message"><b>30 </b><i class="fa fa-envelope-o fa-2x"></i></a>
                <a href="message-task.html" class="btn btn-primary" title="New Task"><b>40 </b><i class="fa fa-bars fa-2x"></i></a>
                <a href="login.html" class="btn btn-danger" title="Logout"><i class="fa fa-exclamation-circle fa-2x"></i></a>

            </div> -->
			
        </nav>
        <br/><br>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <div class="user-img-div">
                            <img src="assets/assets/img/user.png" class="img-thumbnail" />

                            <div class="inner-text">
                                Admin
                            <br />
                            </div>
                        </div>

                    </li>


                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/home"><i class="fa fa-dashboard "></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-pencil "></i>Struktur Kurikulum <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/inputsk"><i class="fa fa-toggle-on"></i>Input Struktur Kurikulum</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lihatSk"><i class="fa fa-tag"></i>lihat Struktur Kurikulum</a>
                            </li>              
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-pencil "></i>Tinjauan Kurikulum <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/inputtk"><i class="fa fa-toggle-on"></i>Input Tinjauan Kurikulum</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/lihattk"><i class="fa fa-tag"></i>lihat Tinjauan Kurikulum</a>
                            </li>              
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-pencil "></i>Data Dosen <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/inputDatadosen"><i class="fa fa-toggle-on"></i>Input Data Dosen</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/datadosen"><i class="fa fa-tag"></i>Lihat Data Dosen</a>
                            </li>              
                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-pencil "></i>Jadwal Kuliah <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/inputJadwal"><i class="fa fa-toggle-on"></i>Input Jadwal Kuliah</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->request->baseUrl; ?>/tampiljadwal"><i class="fa fa-tag"></i>Lihat Jadwal Kuliah</a>
                            </li>              
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->request->baseUrl; ?>/login"><i class="fa fa-lock"></i>Login</a>
                    </li>
                    <div class="tengahin"><font color="white">CopyLeft &copy; By InSup</font></div>
        </div>

        </nav>
		<div id="page-wrapper">
            <div id="page-inner">
			
			
		<?php echo $content; ?>
		
		
		</div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
		<!-- <div id="footer-sec" class="tengahin">
        &copy; 2016 Tim ICT PPs | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
    </div> -->
    <!-- /. FOOTER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/bootstrap.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/jquery.metisMenu.js"></script>
       <!-- CUSTOM SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/assets/js/custom.js"></script>
    


</body>
</html>