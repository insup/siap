<div class="panel panel-info">
    <div class="panel-heading">
        Detail Mata Kuliah
	</div>

    <div class="panel-body">
		<table class="table table-bordered">
			<?php foreach ($hasil as $key): ?>
			<tr>
				<td>Kode Mata Kuliah</td>
				<td><?php echo $key['kode_makul'] ?></td>
			</tr>
			<tr>
				<td>Nama Mata Kuliah</td>
				<td><?php echo $key['mata_kuliah'] ?></td>
			</tr>
			<tr>
				<td>Semester</td>
				<td><?php echo $key['smt'] ?></td>
			</tr>
			<tr>
				<td>Bobot SKS</td>
				<td><?php echo $key['bobot_sks'] ?></td>
			</tr>
			<tr>
				<td>SKS Inti</td>
				<td><?php echo $key['inti'] ?></td>
			</tr>
			<tr>
				<td>SKS Institusional</td>
				<td><?php echo $key['institusional'] ?></td>
			</tr>
			<tr>
				<td>Deskripsi</td>
				<td><?php echo $key['deskripsi'] ?></td>
			</tr>
			<tr>
				<td>Silabus</td>
				<td><?php echo $key['silabus'] ?></td>
			</tr>
			<tr>
				<td>SAP</td>
				<td><?php echo $key['sap'] ?></td>
			</tr>
			<tr>
				<td>Prodi</td>
				<td><?php echo $key['prodi'] ?></td>
			</tr>
			<tr>
				<td>Tahun</td>
				<td><?php echo $key['tahun'] ?></td>
			</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>