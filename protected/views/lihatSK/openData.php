<?php
/* @var $this LihatSKController */

$this->breadcrumbs=array(
	'Lihat Sk',
);
?>
<div class="tengahin">
<h1>Struktur Kurikulum</h1>
<br>
<form action="" name="pr" method="post">
	<select id="prodi" name="prodi">
		<?php foreach ($hasil1 as $key): ?>
			<option value="<?php echo $key['kode_prodi']; ?>"><?php echo $key['nama_prodi'] ?></option>
		<?php endforeach ?>
	</select>
	<input type="submit" name="submit" value="Lihat"></input>
</form>
</div>
<br>
<table class="table table-striped">
	<thead>
		<tr>
			<th>No.</th>
			<th>Prodi</th>
			<th>Nama Mata Kuliah</th>
			<th>SKS</th>
			<th>Semester</th>
			<th colspan="2">Aksi</th>			
		</tr>
	</thead>						
	<tbody>
		<?php $i=0; ?>
		<?php foreach ($hasil as $key): ?>
		<?php $i++; ?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $key['prodi'] ?></td>
		<td class="kiri"><?php echo $key['makul'] ?></td>
		<td><?php echo $key['sks'] ?></td>
		<td><?php echo $key['smt'] ?></td>
		<td><a href="<?php echo Yii::app()->request->baseUrl; ?>/lihatSK/detail/<?php echo $key['id'] ?>" class="btn">Detail</td>
		<td><a href="<?php echo Yii::app()->request->baseUrl; ?>/lihatSK/Hapus/<?php echo $key['id'] ?>" class="btn">Hapus</td>
	</tr>
	</tbody>
	<?php endforeach ?>
</table>