<div class="panel panel-info">
    <div class="panel-heading">
        Detail Tinjauan Mata Kuliah
	</div>

    <div class="panel-body">
		<table class="table table-bordered">
			<?php foreach ($hasil as $key): ?>
			<tr>
				<td>Prodi</td>
				<td><?php echo $key['prodi'] ?></td>
			</tr>
			<tr>
				<td>Nama Mata Kuliah</td>
				<td><?php echo $key['mata_kuliah'] ?></td>
			</tr>
			<tr>
				<td>Status Mata Kuliah</td>
				<td><?php echo $key['status'] ?></td>
			</tr>
			<tr>
				<td>Perubahan pada</td>
				<td><?php echo $key['perubahan'] ?></td>
			</tr>
			<tr>
				<td>Alasan Peninjauan</td>
				<td><?php echo $key['alasan'] ?></td>
			</tr>
			<tr>
				<td>Pengusul Peninjauan</td>
				<td><?php echo $key['pengusul'] ?></td>
			</tr>
			<tr>
				<td>Mulai Berlaku</td>
				<td>Semster <?php echo $key['mulai_smt'] ?>, Tahun <?php echo $key['mulai_tahun'] ?></td>
			</tr>
			<?php endforeach ?>
		</table>
	</div>
</div>