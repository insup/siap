<div class="tengahin">
<h2>Tinjauan Kurikulum</h2>
<br>
<form action="openData/" name="pr" method="post">
	<select id="prodi" name="prodi">
		<?php foreach ($hasil1 as $key): ?>
			<option value="<?php echo $key['kode_prodi']; ?>"><?php echo $key['nama_prodi'] ?></option>
		<?php endforeach ?>
	</select>
	<input type="submit" name="submit" value="Lihat"></input>
</form>
</div>
<br>
<table class="table table-striped">
	<thead>
		<tr>
			<th>No.</th>
			<th>Prodi</th>
			<th>Nama Mata Kuliah</th>
			<th>Status Mata Kuliah</th>
			<th></th>			
		</tr>
	</thead>						
	<tbody>
		<?php $i=0; ?>
		<?php foreach ($hasil as $key): ?>
		<?php $i++; ?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $key['prodi'] ?></td>
		<td class="kiri"><?php echo $key['makul'] ?></td>
		<td><?php echo $key['status'] ?></td>
		<td><a href="<?php echo Yii::app()->request->baseUrl; ?>/lihatTK/detail/<?php echo $key['id'] ?>" class="btn">Detail</td>
	</tr>
	</tbody>
	<?php endforeach ?>
</table>