<?php
/* @var $this LoginController */

$this->breadcrumbs=array(
	'Login',
);
?>
<div class="untukLogin">
	<br/><br/><br/><br/>
	<div id="dalamLogin">
		<img src="assets/assets/img/login.png" class="img-thumbnail" />
		<div class="form">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'enableClientValidation'=>true,
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>

			<div class="row">
				<?php echo $form->labelEx($model,'username'); 
				echo "&nbsp;&nbsp;&nbsp;&nbsp;";
				echo $form->textField($model,'username'); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'password');
				echo "&nbsp;&nbsp;&nbsp;&nbsp;"; ?>
				<?php echo $form->passwordField($model,'password'); ?>
				<?php echo $form->error($model,'password'); ?>
				<p class="hint">
					Hint: You may login with <tt>iin/i-fath</tt>.
				</p>
			</div>

			<div class="row rememberMe">
				<?php echo $form->checkBox($model,'rememberMe'); ?>
				<?php echo $form->label($model,'rememberMe'); ?>
				<?php echo $form->error($model,'rememberMe'); ?>
			</div>

			<div class="row buttons">
				<center><font color="black"><?php echo CHtml::submitButton('Login'); ?></font></center>
			</div>

			<?php $this->endWidget(); ?>
		</div><!-- form -->
	</div>
	<br/>
</div>