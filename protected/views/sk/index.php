<?php
/* @var $this TampiljadwalController */

$this->breadcrumbs=array(
	'SK',
);
?>
<h1>Jadwal Kuliah</h1>

<table class="table table-striped">
	<thead>
		<tr>
			<th>No.</th>
			<th>Nama Mata Kuliah</th>
			<th>Nama Dosen</th>
			<th>Hari</th>
			<th>Jam</th>
			<th>Ruang</th>
			<th>Prodi</th>
		</tr>
	</thead>						
	<tbody>
		<?php $i=0; ?>
		<?php foreach ($hasil as $key): ?>
		<?php $i++; ?>
	<tr>
		<td><?php echo $i;?></td>
		<td><?php echo $key['makul'] ?></td>
		<td><?php echo $key['dsn'] ?></td>
		<td><?php echo $key['hari'] ?></td>
		<td><?php echo $key['waktu'] ?></td>
		<td><?php echo $key['ruang'] ?></td>
		<td><?php echo $key['prodi'] ?></td>
	</tr>
	</tbody>
	<?php endforeach ?>
</table>