<?php
/* @var $this TampiljadwalController */

$this->breadcrumbs=array(
	'Tampil Jadwal',
);
?>
<h1><center>Jadwal Kuliah</center></h1>

<table class="table table-striped">
	<thead>
		<tr>
			<th>No.</th>
			<th>Prodi</th>
			<th>Semester</th>
			<th>Nama Mata Kuliah</th>
			<th>Nama Dosen</th>
			<th>Hari</th>
			<th>Jam</th>
			<th>Ruang</th>
		</tr>
	</thead>						
	<tbody>
		<?php $i=0; ?>
		<?php foreach ($hasil as $key): ?>
		<?php $i++; ?>
	<tr>
		<td><?php echo $i;?></td>
		<td class="kiri"><?php echo $key['prodi'] ?></td>
		<td><?php echo $key['smt'] ?></td>
		<td><?php echo $key['makul'] ?></td>
		<td class="kiri"><?php echo $key['dsn'] ?></td>
		<td><?php echo $key['hari'] ?></td>
		<td><?php echo $key['waktu'] ?></td>
		<td><?php echo $key['ruang'] ?></td>
	</tr>
	</tbody>
	<?php endforeach ?>
</table>